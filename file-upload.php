<?php
$target_dir = 'uploads/';
// upload.php
// 'images' refers to your file input name attribute
if (empty($_FILES['fileUpload'])) {
    echo json_encode(['error'=>'No files found for upload.']); 
    // or you can throw an exception 
    return; // terminate
}

// get the files posted
$image = $_FILES['fileUpload'];

// a flag to see if everything is ok
$success = null;

// file paths to store
$paths= [];

// get file names
$filename = $image['name'];

$ext = explode('.', basename($filename));
$target = $target_dir . $image['name'];
$postImage = $image['name'];
if(move_uploaded_file($image['tmp_name'], $target)) {
    $success = true;
    $paths[] = $target;
} else {
    $success = false;
}

// check and process based on successful status 
if ($success === true) {
    // call the function to save all data to database
    // code for the following function `save_data` is not 
    // mentioned in this example
    //save_data($userid, $username, $paths);

    // store a successful response (default at least an empty array). You
    // could return any additional response info you need to the plugin for
    // advanced implementations.
    $output = ['success'=>'http'.(empty($_SERVER['HTTPS'])?'':'s').'://'.$_SERVER["SERVER_NAME"].'/'.$target];
    // for example you can get the list of files uploaded this way
    // $output = ['uploaded' => $paths];
} elseif ($success === false) {
    $output = ['error'=>'Error while uploading images. Contact the system administrator. '.$postImage ];
    // delete any uploaded files
    foreach ($paths as $file) {
        unlink($file);
    }
} else {
    $output = ['error'=>'No files were processed.'];
}

// return a json encoded response for plugin to process successfully
echo json_encode($output);
?>