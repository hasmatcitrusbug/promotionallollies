<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Branded lollies</title>
		<meta name="description" content="A large choice of branded lollies">
		<meta name="author" content="Total Simplicity">
		<!-- Style -->
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/style.css?ver=1.1" rel="stylesheet">
        <link href="/vendor/fileinput/css/fileinput.min.css" rel="stylesheet">
        <link href="/vendor/fileinput/themes/explorer-fas/theme.css" media="all" rel="stylesheet" type="text/css" />
		<!-- Responsive -->
		<link href="css/responsive.css?ver=1.1" rel="stylesheet">
		<!-- Choose Layout -->
		<link href="css/layout-semiboxed.css" rel="stylesheet">
		<!-- Choose Skin -->
		<link rel="stylesheet" id="main-color" href="css/skin-blue.css" media="screen"/>
		<!-- Favicon -->
		<link rel="shortcut icon" href="img/favicon.ico">
		<!-- IE -->
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="js/respond.min.js"></script>	   
		<![endif]-->
		<!--[if lte IE 8]>
		<link href="css/ie8.css" rel="stylesheet">
		<![endif]-->
	</head>
	<body class="off">
		<!-- /.wrapbox start-->
		<div class="wrapbox">
			<!-- TOP AREA
				================================================== -->
			<section class="toparea">
				<div class="container">
					<div class="row">
						<div class="col-md-8 top-text pull-left animated fadeInLeft">
							<i class="icon-phone"></i> Phone: <a style="padding: 5px; font-size:2em" href="tel:1800 178 888">1800 178 888</a> <span class="separator"></span><i class="icon-envelope"></i> Email: <a href="mailto:sales@promotionallollies.com.au">sales@promotionallollies.com.au</a>
						</div>
						<div class="col-md-4 text-right animated fadeInRight">
							<div class="social-icons">
								<a class="icon icon-facebook" href="#"></a>
								<a class="icon icon-twitter" href="#"></a>
								<a class="icon icon-linkedin" href="#"></a>
								<a class="icon icon-skype" href="#"></a>
								<a class="icon icon-google-plus" href="#"></a>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- /.toparea end-->
			<!-- NAV
				================================================== -->
			<nav class="navbar navbar-fixed-top wowmenu" role="navigation">
				<div class="container">
					<div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand logo-nav" href="/"><img src="img/logo.png" alt="Promotional Lollies"></a><p class="text-center visible-xs" style="margin-top: 15px;"><a href="tel:1800 178 888" style="font-size: 1.5em; font-weight: 900; color: white"><i class="icon-phone"></i> 1800 178 888</a></p>
					</div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav">
                        <li><img src="img/Pointing-Finger-Icon.svg" class="visible-lg" style="width: 30px; margin-left: -40px; margin-top: 16px; position: absolute;"><a href="fast-quote" style="color: #900020;font-size: 1.5em;">GET A FAST QUOTE NOW</a></li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
					
				</div>
			</nav>
        <div class="wrapsemibox">
            <section class="home-portfolio" style="margin-top: 95px;">
                <div class="row">
                    <div class="col-md-12">
                        <div id="response" class="alert-success-custom"  style="display:none">We have received your request, our friendly staff will contact you soon.</div>
						<textarea id="response_data" style="display:none"></textarea>
                        <h1>Fast Quote</h1>
                        <p>To get a fast quote, please submit the template below, and one of our friendly team members will process your request.</p>
                        <form id="contact-form" method="post" action="contact.php" role="form">
                <div class="controls">
					
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_Company">Company</label>
                                <input id="form_Company" type="text" name="company" class="form-control"
                                    placeholder="Please enter your Company name">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_lastname">Name and Surname*</label>
                                <input id="form_lastname" type="text" name="surname" class="form-control"
                                    placeholder="Please enter your first name, and surname *" required="required"
                                    data-error="Lastname is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_email">Email *</label>
                                <input id="form_email" type="email" name="email" class="form-control"
                                    placeholder="Please enter your email *" required="required"
                                    data-error="Valid email is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_Phone">Phone Number *</label>
                                <input id="form_Phone" type="text" name="phone" class="form-control"
                                    placeholder="Please enter your Phone Number *" required="required"
                                    data-error="Phone Number is required.">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_Product">Product Required</label>
                                <input id="form_Product" type="text" name="product" class="form-control"
                                    placeholder="Please enter Required Product">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_Quantity">Quantity Required</label>
                                <input id="form_Quantity" type="text" name="quantity" class="form-control"
                                    placeholder="Please enter Required Quantity">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_Number">Number of Print Colours</label>
                                <input id="form_Number" type="text" name="number" class="form-control"
                                    placeholder="Please enter Number of Print Colours">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_Delivery">Delivery Date Required</label>
                                <input id="form_Delivery" type="text" name="delivery" class="form-control"
                                    placeholder="Please enter Required Delivery Date">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="form_message">Message *</label>
                                <textarea id="form_message" name="message" class="form-control"
                                    placeholder="Message for me *" rows="12" required="required"
                                    data-error="Please, leave us a message."></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Attach Logo/s (optional) 10MB Max:</label>
                                <div class="file-loading">
                                    <!--<input id="fileUpload" name="fileUpload" class="file" type="file" data-theme="fas"
														data-upload-url="file-upload.php"
														data-allowed-file-extensions='["jpg", "jpeg", "png", "gif"]'> -->
									<input id="fileUpload" type="file" name="fileUpload" class="file"  data-overwrite-initial="false" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="submit" id="submit" class="btn btn-primary btn-send" value="Send message">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-muted">
                                <strong>*</strong> These fields are required.</p>
                                <input id="fupload" type="hidden" name="fupload">
                        </div>
                    </div>
                    <div class="messages"></div>
                </div>
                </div></div>
            </form>
            </section>
        </div>
			<div class="footer">
				<div class="container animated fadeInUpNow notransition">
					<div class="row" style="padding-bottom: 30px">
						<div class="col-md-12 text-center">
							<img src="/img/Adstop.png">
					    </div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h4 class="footerbrand"><span class="colortext">B</span>randed Lollies</h4>
							<p>Products illustrated throughout this site are to show personalisation techniques and not to imply that they have been supplied to the owners of the trademark.</p>
							<p>Corporate Lollies are only for sale to the owner of that particular trademark or logo.</p>
					    </div>
						<div class="col-md-9">
						<div class="col-md-4">
							<h4 class="title"><span class="colortext">B</span>risbane Branch</h4>
							<div class="">
								<p>South Brisbane<br>Brisbane<br>QLD 4101</p>
							</div>
						</div>
						<div class="col-md-4">
							<h4 class="title"><span class="colortext">S</span>ydney Branch</h4>
							<div class="">
								<p>Erskineville<br>Sydney<br>NSW 2043</p>
							</div>
						</div>
						<div class="col-md-4">
								<h4 class="title"><span class="colortext">M</span>elbourne Branch</h4>
								<div class="">
									<p>Mitcham<br>Melbourne<br>VIC 3132</p>
								</div>
							</div>
							<div class="col-md-12" style="margin-top: 10px">
						<p class="text-center" style="font-size:1.3em">
								<strong>Phone: </strong> <a href="tel:1800 178 888">1800 178 888</a>
							</p>

				<p class="text-center" style="font-size:1.3em">
								<strong>Email: </strong> <a href="mailto:sales@promotionallollies.com.au">sales@promotionallollies.com.au</a>
							</p>
							<ul class="social-icons list-soc text-center">
									<li><a href="#"><i class="icon-facebook"></i></a></li>
									<li><a href="#"><i class="icon-twitter"></i></a></li>
									<li><a href="#"><i class="icon-linkedin"></i></a></li>
									<li><a href="#"><i class="icon-google-plus"></i></a></li>
									<li><a href="#"><i class="icon-skype"></i></a></li>
								</ul>
								</div>
							</div>
					</div>
				</div>
			</div>
			<p id="back-top">
				<a href="#top"><span></span></a>
			</p>
			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<p class="pull-left" style="font-size:0.9em">
								Delivery of Branded Lollies across Australia, including Brisbane, Sydney, Melbourne, Perth, Adelaide and Darwin.
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- /footer section end-->
		</div>

		<!-- SCRIPTS, placed at the end of the document so the pages load faster
			================================================== -->
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.js"></script>
		<script src="js/plugins.js"></script>
		<script src="js/common.js"></script>
        <script src="/vendor/fileinput/js/plugins/piexif.js" type="text/javascript"></script>
        <script src="/vendor/fileinput/js/fileinput.min.js"></script>
        <script src="/vendor/fileinput/themes/fas/theme.js" type="text/javascript"></script>
        <script src="/vendor/fileinput/themes/explorer-fas/theme.js" type="text/javascript"></script>
		<script>
			/* ---------------------------------------------------------------------- */
			/*	Carousel
			/* ---------------------------------------------------------------------- */
			$(window).load(function(){			
				$('#carousel-projects').carouFredSel({
				responsive: true,
				items       : {
			       width       : 200,
			       height      : 295,
			       visible     : {
			           min         : 1,
			           max         : 4
			       }
			   },
				width: '200px',
				height: '295px',
				auto: true,
				circular	: true,
				infinite	: false,
				prev : {
					button		: "#car_prev",
					key			: "left",
						},
				next : {
					button		: "#car_next",
					key			: "right",
							},
				swipe: {
					onMouse: true,
					onTouch: true
					},
				scroll: {
			       easing: "",
			       duration: 1200
			   }
			});
				});
			
			$("#fileUpload").fileinput({
				theme: 'fa',
				uploadUrl: "/file-upload.php",
				allowedFileExtensions: ["jpg", "jpeg", "png", "gif"],
				overwriteInitial: false,
				maxFileSize: 2000,
				maxFilesNum: 1,
				maxFileCount : 1,
				uploadAsync: false,
				slugCallback: function (filename) {
					return filename.replace('(', '_').replace(']', '_');
				}
			}).on('fileselect', function() {
				$('.fileinput-upload-button').css({ display: 'none' });
				$('.fileinput-upload-button').click(); // upload the file that has been selected
			}).on('filebatchuploadsuccess', function(event, data) {
				console.log(event,data);
				$("#fupload").val(data.response.success);
			});

			$('#submit').click(function() {
			
				window.scrollTo(0, 0);

				/*var form = $('#contact-form')[0];
				var text = form.serialize();
				console.log('form:', text);*/
			})
		</script>
		<script>
			//CALL TESTIMONIAL ROTATOR
			$( function() {
				/*
				- how to call the plugin:
				$( selector ).cbpQTRotator( [options] );
				- options:
				{
					// default transition speed (ms)
					speed : 700,
					// default transition easing
					easing : 'ease',
					// rotator interval (ms)
					interval : 8000
				}
				- destroy:
				$( selector ).cbpQTRotator( 'destroy' );
				*/
				$( '#cbp-qtrotator' ).cbpQTRotator();
			} );
		</script>
		<script>
			//CALL PRETTY PHOTO
			$(document).ready(function(){
				$("a[data-gal^='prettyPhoto']").prettyPhoto({social_tools:'', animation_speed: 'normal' , theme: 'dark_rounded'});
			});
		</script>
		<script>
			//MASONRY
			$(document).ready(function(){
			var $container = $('#content');
			  $container.imagesLoaded( function(){
				$container.isotope({
				filter: '*',	
				animationOptions: {
				 duration: 750,
				 easing: 'linear',
				 queue: false,	 
			   }
			});
			});
			$('#filter a').click(function (event) {
				$('a.selected').removeClass('selected');
				var $this = $(this);
				$this.addClass('selected');
				var selector = $this.attr('data-filter');
				$container.isotope({
					 filter: selector
				});
				return false;
			});
			});
		</script>
		<script>
			//ROLL ON HOVER
				$(function() {
				$(".roll").css("opacity","0");
				$(".roll").hover(function () {
				$(this).stop().animate({
				opacity: .8
				}, "slow");
				},
				function () {
				$(this).stop().animate({
				opacity: 0
				}, "slow");
				});
				});
		</script>
	</body>
</html>